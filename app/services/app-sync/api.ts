/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateCountingSessionInput = {
  id?: string | null,
  status: SessionStatus,
};

export enum SessionStatus {
  ACTIVE = "ACTIVE",
  CLOSED = "CLOSED",
}


export type ModelCountingSessionConditionInput = {
  status?: ModelSessionStatusInput | null,
  and?: Array< ModelCountingSessionConditionInput | null > | null,
  or?: Array< ModelCountingSessionConditionInput | null > | null,
  not?: ModelCountingSessionConditionInput | null,
};

export type ModelSessionStatusInput = {
  eq?: SessionStatus | null,
  ne?: SessionStatus | null,
};

export type CountingSession = {
  __typename: "CountingSession",
  id: string,
  status: SessionStatus,
  createdAt: string,
  updatedAt: string,
};

export type UpdateCountingSessionInput = {
  id: string,
  status?: SessionStatus | null,
};

export type DeleteCountingSessionInput = {
  id: string,
};

export type ModelCountingSessionFilterInput = {
  id?: ModelIDInput | null,
  status?: ModelSessionStatusInput | null,
  and?: Array< ModelCountingSessionFilterInput | null > | null,
  or?: Array< ModelCountingSessionFilterInput | null > | null,
  not?: ModelCountingSessionFilterInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelCountingSessionConnection = {
  __typename: "ModelCountingSessionConnection",
  items?:  Array<CountingSession | null > | null,
  nextToken?: string | null,
};

export type CreateCountingSessionMutationVariables = {
  input: CreateCountingSessionInput,
  condition?: ModelCountingSessionConditionInput | null,
};

export type CreateCountingSessionMutation = {
  createCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type UpdateCountingSessionMutationVariables = {
  input: UpdateCountingSessionInput,
  condition?: ModelCountingSessionConditionInput | null,
};

export type UpdateCountingSessionMutation = {
  updateCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type DeleteCountingSessionMutationVariables = {
  input: DeleteCountingSessionInput,
  condition?: ModelCountingSessionConditionInput | null,
};

export type DeleteCountingSessionMutation = {
  deleteCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type GetCountingSessionQueryVariables = {
  id: string,
};

export type GetCountingSessionQuery = {
  getCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type ListCountingSessionsQueryVariables = {
  filter?: ModelCountingSessionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListCountingSessionsQuery = {
  listCountingSessions?:  {
    __typename: "ModelCountingSessionConnection",
    items?:  Array< {
      __typename: "CountingSession",
      id: string,
      status: SessionStatus,
      createdAt: string,
      updatedAt: string,
    } | null > | null,
    nextToken?: string | null,
  } | null,
};

export type OnCreateCountingSessionSubscription = {
  onCreateCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnUpdateCountingSessionSubscription = {
  onUpdateCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};

export type OnDeleteCountingSessionSubscription = {
  onDeleteCountingSession?:  {
    __typename: "CountingSession",
    id: string,
    status: SessionStatus,
    createdAt: string,
    updatedAt: string,
  } | null,
};
