/**
 * Welcome to the main entry point of the app. In this file, we'll
 * be kicking off our app.
 *
 * Most of this file is boilerplate and you shouldn't need to modify
 * it very often. But take some time to look through and understand
 * what is going on here.
 *
 * The app navigation resides in ./app/navigators, so head over there
 * if you're interested in adding screens and navigators.
 */
import "./i18n"
import "./utils/ignore-warnings"
import React, { useState, useEffect, useRef } from "react"
import { NavigationContainerRef } from "@react-navigation/native"
import { SafeAreaProvider, initialWindowMetrics } from "react-native-safe-area-context"
import { initFonts } from "./theme/fonts" // expo
import * as storage from "./utils/storage"
import {
  useBackButtonHandler,
  RootNavigator,
  canExit,
  setRootNavigation,
  useNavigationPersistence,
} from "./navigators"
import { RootStore, RootStoreProvider, setupRootStore } from "./models"
import { ToggleStorybook } from "../storybook/toggle-storybook"

import Amplify, { API, graphqlOperation } from "aws-amplify"
import config from "../src/aws-exports"
import { withAuthenticator } from "aws-amplify-react-native"
import { getCountingSession } from "../src/graphql/queries"

// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator
import { enableScreens } from "react-native-screens"
import { Linking } from "react-native"
import { Alert } from "react-native"
import { Platform } from "react-native"
enableScreens()

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

Amplify.configure(config)

/**
 * This is the root component of our app.
 */
function App() {
  const navigationRef = useRef<NavigationContainerRef>(null)
  const [rootStore, setRootStore] = useState<RootStore | undefined>(undefined)

  setRootNavigation(navigationRef)
  useBackButtonHandler(navigationRef, canExit)
  const { initialNavigationState, onNavigationStateChange } = useNavigationPersistence(
    storage,
    NAVIGATION_PERSISTENCE_KEY,
  )

  // Kick off initial async loading actions, like loading fonts and RootStore
  useEffect(() => {
    ;(async () => {
      await initFonts() // expo
      setupRootStore().then(setRootStore)

      Platform.OS === "android" && resolveSessionFromPlayStoreReferrer()

      const url = await Linking.getInitialURL()
      handleLink(url)
      Linking.addEventListener("url", (payload) => handleLink(payload.url))

      testFetch()
    })()

    return () => {
      Linking.removeAllListeners("url")
    }
  }, [])

  const testFetch = async () => {
    const { data } = await API.graphql(
      graphqlOperation(
        `
        query allPosts {
          allPosts {
              id
              author
              title
              content
              url
              ups
              downs
              relatedPosts {
                  id
                  title
              }
          }
      }
  `,
      ),
    )

    console.log(data)
  }

  const resolveSessionFromPlayStoreReferrer = () => {
    let sessionId

    // Check for session ID in Play Store referrer
    import("react-native-play-install-referrer").then(({ PlayInstallReferrer }) => {
      PlayInstallReferrer.getInstallReferrerInfo((installReferrerInfo, error) => {
        if (!error) {
          sessionId = installReferrerInfo.installReferrer
          console.log("sessionId from Play Store referrer", sessionId)
          startCounting(sessionId)
        } else {
          console.log("Failed to get install referrer info!", error.toString())
        }
      })
    })
  }

  const handleLink = (link) => {
    // Alert.alert("handle dynamic link", link)
    if (link) {
      const linkElements = link.split("=")
      const sessionId = linkElements[linkElements.length - 1]
      console.log("session ID", sessionId)
      startCounting(sessionId)
    }
  }

  const startCounting = async (sessionId) => {
    const input = { id: sessionId }
    const { data } = await API.graphql(graphqlOperation(getCountingSession, input))
    const session = data.getCountingSession
    if (session && session.status === "ACTIVE") Alert.alert("Starting counting session")
    else Alert.alert("No active session found")
  }

  // Before we show the app, we have to wait for our state to be ready.
  // In the meantime, don't render anything. This will be the background
  // color set in native by rootView's background color. You can replace
  // with your own loading component if you wish.
  if (!rootStore) return null

  // otherwise, we're ready to render the app
  return (
    <ToggleStorybook>
      <RootStoreProvider value={rootStore}>
        <SafeAreaProvider initialMetrics={initialWindowMetrics}>
          <RootNavigator
            ref={navigationRef}
            initialState={initialNavigationState}
            onStateChange={onNavigationStateChange}
          />
        </SafeAreaProvider>
      </RootStoreProvider>
    </ToggleStorybook>
  )
}

export default App
// export default withAuthenticator(App)
